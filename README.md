# Simple Revenue Agency System 

Development of a Web-application for a simplified version of the Revenue Agency system.

## Tools:
1. Programming Language:
     - Java (using jdk 11)
2. Application Framework:
     - Spring Framework / Spring Boot
3. Persistence:
     - Spring Data and Hibernate
     - H2 database
4. Presentation:
     - Thymeleaf - templating engine
     - HTML, CSS
5. Build:
     - Maven - build automation and dependency management

## Info
The application is made up of two parts - a console application with which we can import data into a database,
and a web application, with which the user can easily manage the data and use the functionality of the application,
by filling in text boxes, navigating through buttons, and printing data into forms in web pages.

## Test
 * testdata/data.txt - data for testing the console application

## Console Application
The first line of the console will be a `N` followed by `N * 3` lines, with each three lines representing
the "file" of a person. Every three lines looks like this:

1. First row:
    * First and last name - Free text
    * Gender - one letter `M` or `F`
    * Height - a positive integer
    * Date of birth
    * Country, city, municipality, post code, street name, street number - Free text
    
2. Second row - Education data will be entered (if any).
    * Type - one letter `P`, `S`, `B`, `M` or `D`
        * Primary
        * Secondary
        * Bachelor
        * Master
        * Doctorate
    * Institution name - Free text
    * Enrollment date
    * Graduation date (if graduated)
    * Whether completed - `YES` or `NO`
    * Grade, if education is completed or an empty string if the education is primary or incomplete
    
3. Third row - social insurance data will be entered (if any).
    * Amount
    * Year
    * Month

*All dates are in the format `d.M.yyyy` (`1.2.2003`, `11.1.2014`, `12.12.2012`).*

```
<first_name>;<last_name>;<gender>;<height>;<birthday>;<country>;<city>;<municipality>;<postcode>;<street_name>;<street_number>
<education_type>;<institution_name>;<enrollment_date>;<graduation_date>;<has_graduated>;[<final_grade>]
<amount>;<year>;<month>
```

## Web Application

The web application uses the MVC model and *Thymeleaf* template engine.

There are two html pages `people.html` and `personDetails.html`. The first one is  "home" page, from which you can navigate to the other. 
In `people.html` there is a list with all people in the database, displayed only by first and last name. 
The names are links to `personDetails.html`, where detailed information of the person will be represented. 
`people.html` should also have two text fields and a button that form a search form. In both fields, the user is able to enter 
the first and / or last name respectively of a person, and the data will be filtered according to these criteria. 
For example:

List of people in database: 
* Петър Петров
* Иван Иванов
* Петър Иванов

If the user enters "Петър Иванов", must receive only the last result. If they enter "Петър",
the result must be the first and the last person and if it was entered "Иванов", the result must be the second and the last person.

In the second page `personDetails.html`, the user sees "Go back" at the top, that leads to the first page. 
Under "Go back" there is personal information for the person.

On the last line of the personal details table, there is a button named `Get Social Aid Amount`, which when
pressed disappears and in its place a text showing the value of the social benefits a person is entitled appears 
or 'NOT ELIGIBLE' if he / she is not entitled to such. A person is entitled to social aid if:

* has completed secondary education
* has not been insured for the last 3 months

The value of social benefits is equal to the sum of all social insurances in the last 27 months divided by 24.