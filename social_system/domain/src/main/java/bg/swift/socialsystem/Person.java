package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.SecondaryEducation;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.*;


@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private static final LocalDate EARLIEST_DATE_ALLOWED = LocalDate.of(1900, 1, 1);

    private LocalDate birthday;

    private String firstName;
    private String lastName;

    private String country, city, municipality, streetName, streetNumber, postCode;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private int height;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Education> educations;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SocialInsuranceRecord> socialInsuranceRecords;

    Person() {

    }

    Person(String firstName, String lastName, Gender gender, int height, LocalDate birthday,
           String country, String city, String municipality, String postCode, String streetName, String streetNumber) {

        validateBirthday(birthday);
        validateName(firstName, "first");
        validateName(lastName, "last");
        validateHeight(height);

        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.birthday = birthday;
        this.country = country;
        this.city = city;
        this.municipality = municipality;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.postCode = postCode;
        this.socialInsuranceRecords = new LinkedHashSet<>();
        this.educations = new LinkedHashSet<>();
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public Set<SocialInsuranceRecord> getSocialInsuranceRecords() {
        return socialInsuranceRecords;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getMunicipality() {
        return municipality;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public Integer getId() {
        return id;
    }

    public Gender getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public int getHeight() {
        return height;
    }

    void addEducations(Set<Education> educations) {
        this.educations.addAll(educations);
    }

    void addSocialInsuranceRecord(Set<SocialInsuranceRecord> record) {
        this.socialInsuranceRecords.addAll(record);
    }

    private void validateName(String firstName, String propertyName) {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Expected non-empty " + propertyName + " name.");
        }
    }

    private void validateHeight(int height) {
        if (height < 40 || 300 < height) {
            throw new IllegalArgumentException("Expected height is between 40 and 300 cm.");
        }
    }

    private void validateBirthday(LocalDate birthday) {
        if (birthday.isBefore(EARLIEST_DATE_ALLOWED) || birthday.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Date of birth is expected to be after 01.01.1900 and before now.");
        }
    }

    public Optional<Double> getSocialAidAmount() {

        boolean doNotHaveSecondaryEducation = true;

        for (Education e : this.educations) {
            if (e instanceof SecondaryEducation && e.hasGraduated()) {
                doNotHaveSecondaryEducation = false;
                break;
            }
        }

        if (doNotHaveSecondaryEducation) {
            return Optional.empty();
        }

        List<SocialInsuranceRecord> records = new ArrayList<>();

        for (SocialInsuranceRecord r : this.socialInsuranceRecords) {
            if (r.recordIsAfter(YearMonth.now().minusMonths(27))) {
                records.add(r);
            }
        }

        boolean hasNoInsuranceRecordsWithinLast27Months = records.isEmpty();

        if (hasNoInsuranceRecordsWithinLast27Months) {
            return Optional.empty();
        }

        boolean hasBeenInsuredInLast3Months = false;

        for (SocialInsuranceRecord e : records) {
            if (e.recordIsAfter(YearMonth.now().minusMonths(3))) {
                hasBeenInsuredInLast3Months = true;
                break;
            }
        }

        if (hasBeenInsuredInLast3Months) {
            return Optional.empty();
        }

        double sum = 0;

        for (SocialInsuranceRecord r : records) {
            sum += r.getAmount();
        }

        sum /= 24;
        sum = Math.round(sum * 100);
        return Optional.of(sum / 100);
    }
}
