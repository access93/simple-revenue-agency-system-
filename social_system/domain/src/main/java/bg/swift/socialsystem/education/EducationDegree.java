package bg.swift.socialsystem.education;

public enum EducationDegree {

    NONE('N'),
    PRIMARY('P'),
    SECONDARY('S'),
    BACHELOR('B'),
    MASTER('M'),
    DOCTORATE('D');

    private char abbreviation;

    EducationDegree(char abbreviation) {
        this.abbreviation = abbreviation;
    }

    EducationDegree() {

    }

    public static EducationDegree of(char abbreviation) {

        for (EducationDegree degree : EducationDegree.values()) {
            if (degree.abbreviation == abbreviation) {
                return degree;
            }
        }

        throw new IllegalArgumentException("Unrecognized education code.");
    }
}
