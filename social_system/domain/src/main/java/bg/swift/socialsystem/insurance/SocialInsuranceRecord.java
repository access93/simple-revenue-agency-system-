package bg.swift.socialsystem.insurance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.YearMonth;

@Entity
public class SocialInsuranceRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double amount;
    private YearMonth yearMonth;

    SocialInsuranceRecord() {

    }

    public SocialInsuranceRecord(double amount, YearMonth yearMonth) {
        this.amount = amount;
        this.yearMonth = yearMonth;
    }

    public double getAmount() {
        return amount;
    }

    public YearMonth getYearMonth() {
        return yearMonth;
    }

    public boolean recordIsAfter(YearMonth yearMonth) {
        return this.yearMonth.isAfter(yearMonth);
    }
}
