package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public abstract class GradedEducation extends Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double finalGrade;

    GradedEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate) {
        super(institutionName, enrollmentDate, graduationDate);
    }

    GradedEducation() {

    }

    public void graduate(double finalGrade) {

        if (finalGrade < 2 || 6 < finalGrade) {
            throw new IllegalArgumentException("Graduation grade is expected to be between 2 and 6.");
        }

        super.graduate();
        this.finalGrade = finalGrade;
    }

    public double getFinalGrade() {

        if (hasGraduated()) {
            return finalGrade;
        }

        throw new IllegalStateException("No final grade can be provided before graduation.");
    }
}
