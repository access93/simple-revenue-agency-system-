package bg.swift.socialsystem;

import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.stereotype.Component;

import java.time.YearMonth;
import java.util.Comparator;

@Component
public class SocialInsuranceComparator implements Comparator<SocialInsuranceRecord> {

    @Override
    public int compare(SocialInsuranceRecord one, SocialInsuranceRecord another) {

        YearMonth yearMonthOne = YearMonth.of(one.getYearMonth().getYear(), one.getYearMonth().getMonth());
        YearMonth yearMonthAnother = YearMonth.of(another.getYearMonth().getYear(), another.getYearMonth().getMonth());

        return yearMonthAnother.compareTo(yearMonthOne);
    }
}