package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class EducationComparator implements Comparator<Education> {

    @Override
    public int compare(Education one, Education another) {
        return another.getGraduationDate().compareTo(one.getGraduationDate());
    }
}
