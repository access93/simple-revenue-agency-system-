package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class WebApplication {

    private PersonRepository personRepository;
    private final SocialInsuranceComparator socialInsuranceComparator;
    private final EducationComparator educationComparator;


    public WebApplication(PersonRepository personRepository, SocialInsuranceComparator socialInsuranceComparator, EducationComparator educationComparator) {
        this.personRepository = personRepository;
        this.socialInsuranceComparator = socialInsuranceComparator;
        this.educationComparator = educationComparator;
    }

    @GetMapping("/people")
    public String getPeople(Model model, @RequestParam(value = "firstName", defaultValue = "%", required = false) String firstName, @RequestParam(value = "lastName", defaultValue = "%", required = false) String lastName) {

        List<Person> filteredPeople;

        if (firstName.equals("%") && lastName.equals("%")) {
            filteredPeople = personRepository.findAll();
        } else if (!firstName.equals("%") && lastName.equals("%")) {
            filteredPeople = getPeopleByFirstName(firstName);
        } else if (firstName.equals("%")) {
            filteredPeople = getPeopleByLastName(lastName);
        } else {
            filteredPeople = getPeopleByFirstAndLastName(firstName, lastName);
        }

        model.addAttribute("people", filteredPeople);

        return "people";
    }

    @GetMapping("/people/{id}")
    public String getPersonBy(@PathVariable("id") Integer id, Model model) {

        Person person = personRepository.findById(id).get();

        List<Education> sortedEducations = new ArrayList<>(person.getEducations());
        sortedEducations.sort(educationComparator);

        List<SocialInsuranceRecord> sortedInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        sortedInsuranceRecords.sort(socialInsuranceComparator);

        model.addAttribute("person", person);
        model.addAttribute("sortedEducations", sortedEducations);
        model.addAttribute("sortedInsuranceRecords", sortedInsuranceRecords);

        return "person-details";
    }

    @PostMapping("/people/{id}")
    public String addNewInsurance(@PathVariable("id") Integer id, Model model, Double amount, Integer year, Integer month) {

        Person person = personRepository.findById(id).get();
        Set<SocialInsuranceRecord> tmp = Set.of(new SocialInsuranceRecord(amount, YearMonth.of(year, month)));
        person.addSocialInsuranceRecord(tmp);
        personRepository.save(person);

        List<Education> sortedEducations = new ArrayList<>(person.getEducations());
        sortedEducations.sort(educationComparator);

        List<SocialInsuranceRecord> sortedInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        sortedInsuranceRecords.sort(socialInsuranceComparator);

        model.addAttribute("person", person);
        model.addAttribute("sortedEducations", sortedEducations);
        model.addAttribute("sortedInsuranceRecords", sortedInsuranceRecords);

        return "person-details";
    }

    private List<Person> getPeopleByFirstName(String firstName) {
        return personRepository.findByFirstName(firstName);
    }

    private List<Person> getPeopleByLastName(String lastName) {
        return personRepository.findByLastName(lastName);
    }

    private List<Person> getPeopleByFirstAndLastName(String firstName, String lastName) {
        return personRepository.findByFirstNameAndLastName(firstName, lastName);
    }
}
